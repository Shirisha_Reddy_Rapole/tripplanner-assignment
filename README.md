##Goal: Build a simple app that will let you compute travel times.

###Requirements:
-You need an image that you will use as the background. Make is something
travel related. Use the android:background property of the layout to set the
image. Hint: Look for/crop an image to be about 480 by 640 pixels.  
-You will need 3 EditTexts,a TextView and a Button. For each of the text
components, you may want to set the background color. I did
android:background="@android:color/holo_blue_bright"
but there are plenty of other color choices out there. If you want to change the color of the
hint, set the android:textColorHint property.  
a. The first edit text allows entry of a distance in miles. (Double)  
b. The second edit text allows entry of a speed in miles/hour. (Double)  
c. The third edit text allows entry of the number of 15 minute breaks.
(Int)  
d. The text view is where you will display the total time for the trip in
hours.  
e. Press the button to compute the time.  
-In the button method, use a single try-catch for NumberFormatException to
prevent the app from crashing. If the exception happens, put a message in
the text view.  
-Make sure the label of the app in the manifest is "Trip Planner"  
###Bonus:
Ø Add extra descriptive text views to give the user instructions on how to use
the app.  
Ø Use appropriate text entry components to prevent the user from typing nonnumeric
strings.  
Ø Add a reset button that clears all the text.  
Ø Change the look of the edit text with an image.  
Ø Add an edit text that will allow the user to enter the time for a break.  
Ø Add a button that will let you switch back and forth between calculations in
the miles/hour and feet/second. In both cases, the final time should be
displayed in hours. Add a TextView so that we know which system is in use.  
Ø Change the total time text view to be an edit text. Add a button that will
compute the required speed given the other values. If you want to be really
fancy, determine which of the entries is empty and compute it. (Note, some
combinations of values may not have a solution.)  