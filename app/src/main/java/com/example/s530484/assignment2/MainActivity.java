package com.example.s530484.assignment2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void timeComputation(View v)
    {
        EditText distance=(EditText)findViewById(R.id.distance);
        EditText speed=(EditText)findViewById(R.id.speed);
        EditText stops=(EditText)findViewById(R.id.stops);
        EditText breakTime=(EditText)findViewById(R.id.breakTime);
        TextView result=(TextView)findViewById(R.id.time);
        TextView measure=(TextView)findViewById(R.id.measure);

        try {
            double dist = new Double(distance.getText().toString());
            double sp = new Double(speed.getText().toString());
            int stop = new Integer(stops.getText().toString());
            int brkTime=new Integer(breakTime.getText().toString());
            double calc;
            calc = dist / sp;
            calc = (calc) + (stop * (brkTime/60.0));
            calc = Math.round(calc);
            String res = String.valueOf(calc);
            result.setText(res + " hours");
            measure.setText("miles/hour");
        }
        catch(NumberFormatException ex)
        {
            result.setText("Invalid input");
        }
    }
    public void feetFunction(View v)
    {
        EditText distance=(EditText)findViewById(R.id.distance);
        EditText speed=(EditText)findViewById(R.id.speed);
        EditText stops=(EditText)findViewById(R.id.stops);
        EditText breakTime=(EditText)findViewById(R.id.breakTime);
        TextView result=(TextView)findViewById(R.id.time);
        TextView measure=(TextView)findViewById(R.id.measure);

        try {
            double dist = new Double(distance.getText().toString());
            double sp = new Double(speed.getText().toString());
            int stop = new Integer(stops.getText().toString());
            int brkTime=new Integer(breakTime.getText().toString());
            double calc;
            calc = dist / sp;
            calc = calc/(3600.0);
            calc = (calc) + (stop * (brkTime/60.0));
            calc = Math.round(calc);
            String res = String.valueOf(calc);
            result.setText(res + " hours");
            measure.setText("feet/second");
        }
        catch(NumberFormatException ex)
        {
            result.setText("Invalid input");
        }
    }
    public void resetFunction(View v)
    {
        EditText distance=(EditText)findViewById(R.id.distance);
        EditText speed=(EditText)findViewById(R.id.speed);
        EditText stops=(EditText)findViewById(R.id.stops);
        EditText breakTime=(EditText)findViewById(R.id.breakTime);
        TextView result=(TextView)findViewById(R.id.time);

        distance.setText("");
        speed.setText("");
        stops.setText("");
        breakTime.setText("");
        result.setText("");
    }
}
